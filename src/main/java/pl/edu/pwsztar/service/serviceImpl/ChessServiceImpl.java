package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame pawn;
    private RulesOfGame king;
    private RulesOfGame queen;
    private RulesOfGame rock;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("Pawn") RulesOfGame pawn,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rock") RulesOfGame rock){
        this.bishop = bishop;
        this.knight = knight;
        this.pawn   = pawn;
        this.king   = king;
        this.queen  = queen;
        this.rock   = rock;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto) {
        boolean correctmove = false;
        int startcolumn = figureMoveDto.getStart().charAt(0);
        int destinationcolumn = figureMoveDto.getDestination().charAt(0);
        int startrow = figureMoveDto.getStart().charAt(2);
        int destinationrow = figureMoveDto.getDestination().charAt(2);

        if(figureMoveDto.getType()== FigureType.BISHOP){
            correctmove=bishop.isCorrectMove(startrow,startcolumn,destinationrow,destinationcolumn);
        }
        if(figureMoveDto.getType()== FigureType.KNIGHT){
            correctmove=knight.isCorrectMove(startrow,startcolumn,destinationrow,destinationcolumn);
        }
        if(figureMoveDto.getType()== FigureType.PAWN){
            correctmove=pawn.isCorrectMove(startrow,startcolumn,destinationrow,destinationcolumn);
        }
        if(figureMoveDto.getType()== FigureType.KING){
            correctmove=king.isCorrectMove(startrow,startcolumn,destinationrow,destinationcolumn);
        }
        if(figureMoveDto.getType()== FigureType.QUEEN){
            correctmove=queen.isCorrectMove(startrow,startcolumn,destinationrow,destinationcolumn);
        }
        if(figureMoveDto.getType()== FigureType.ROCK){
            correctmove=rock.isCorrectMove(startrow,startcolumn,destinationrow,destinationcolumn);
        }
        return correctmove;
    }
}
